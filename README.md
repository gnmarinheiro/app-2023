# Test de recrutement pour apprenti(e) informaticien(ne) 2023

## Bienvenue ! 
Intéressé.e par rejoindre les équipes de EVA en apprentissage ? 

Bravo ! Tu es au bon endroit.

Afin de valider ton intérêt pour le poste, nous te proposer de passer ce petit test technique.

## Objectif : Réaliser une application de génération de devis
Toutes les informations sont dans le PDF à la racine de ce repo.

![Bonne Chance](https://i.ibb.co/RhqvDmY/C3k-Pzq-CWAAA8g-Rc.jpg)